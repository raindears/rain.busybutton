function index(enviroment, callback, context) {
  var data = {
    label: context.label,
    type: context.type || "primary",
    isPrimary: !!(context.type !== "secondary"),
    busy: context.busy || false,
    state: context.state || "enabled",
    class: context.class
  };

  callback(null, data);
}


module.exports = {
  index: index
};