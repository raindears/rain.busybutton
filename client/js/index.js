define([], function() {

  /**
   * A reusable button component that supports primary and secondary button types,
   * showing a loading indicator,
   * setting their labels and enabling and disabling them.
   *
   * Handlebar example for a secondary button that shows a loading indicator:
   * <code>{{component name="busybutton" view="index" type="secondary" label="foo" busy=true sid="myBusyButton"}}</code>
   *
   * Example to remove the loading indicator in your controller:
   * <code>busyButton.setBusy(false);</code>
   *
   * @name BusyButton
   * @constructor
   */
  function BusyButton() {}

  BusyButton.prototype = {

    init: function() {},

    start: function() {
      this.$button = this.context.getRoot().find('button');
    },

    /**
     * Shorthand to configure the button
     *
     * @param {Map} options The options ("label", "type", "busy", "state")
     */
    configure: function (options) {
      this.__throwIfNotStarted("configure");

      this.setLabel(options.label);
      this.setType(options.type);
      this.setBusy(options.busy);
      this.setState(options.state);

      return this;
    },

    /**
     * Sets the label of the button
     *
     * @param {String} label The label text
     */
    setLabel: function(label) {
      this.__throwIfNotStarted("setLabel");

      if (label || label === "") {
        var labelElem = this.$button.find('span');
        labelElem.text(label);
      }
    },

    /**
     * Sets the label of the button
     *
     * @param {String} type The button type ("primary" or "secondary")
     */
    setType: function(type) {
      this.__throwIfNotStarted("setType");

      if (type === "primary") {
        this.$button.addClass("primary");
        this.$button.removeClass("secondary");
      }
      else if (type === "secondary") {
        this.$button.addClass("secondary");
        this.$button.removeClass("primary");
      }
    },

    /**
     * Shows a loading indicator
     *
     * @param {Boolean} busy Whether to show the loading indicator or not
     */
    setBusy: function(busy) {
      this.__throwIfNotStarted("setBusy");

      if (busy === true) {
        this.$button.addClass("busy");
      }
      else if (busy === false) {
        this.$button.removeClass("busy");
      }
    },

    /**
     * Sets the button busy if not yet busy and vice versa.
     */
    toggleBusy: function() {
      this.__throwIfNotStarted("toggleBusy");

      this.setBusy(!this.$button.hasClass("busy"));
    },

    /**
     * Disables or enables the button
     *
     * @param {String} state The state ("enabled" or "disabled")
     */
    setState: function(state) {
      this.__throwIfNotStarted("setState");

      if (state === "disabled") {
        this.disable();
      }
      else if (state === "enabled") {
        this.enable();
      }
    },

    /**
     * Convenience method to disable or enable the button
     *
     * @param {Boolean} enable Whether to enable or disable the button
     */
    setEnabled: function(enable) {
      if (enable) {
        this.enable();
      }
      else {
        this.disable();
      }
    },

    /**
     * Enables the button
     */
    enable: function() {
      this.__throwIfNotStarted("enable");

      this.$button.removeAttr("disabled");
      this.$button.removeClass("disabled");
      this.$button.addClass("enabled");
    },

    /**
     * Disables the button
     */
    disable: function() {
      this.__throwIfNotStarted("disable");

      this.$button.attr("disabled", "disabled");
      this.$button.addClass("disabled");
      this.$button.removeClass("enabled");
    },

    __throwIfNotStarted: function(methodName) {
      if (!this.$button) {
        throw new RainError(methodName + '() cannot be called before start is executed',
          RainError.ERROR_PRECONDITION_FAILED);
      }
    }
  };

  return BusyButton;

});
